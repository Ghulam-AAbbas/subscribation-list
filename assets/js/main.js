$(function() {
  "use strict"; 
  /*-------------------------------------
            HEADER SCRIPTS 
  ---------------------------------------*/
	$('.header-area .top-nav').each(function(){
		var height_ = $(this).outerHeight();
		$('.header-area').append('<div class="space-header">');
		$('.header-area .space-header').css('height', height_); 
	});
	$(".nav-link.has-child").click(function() {
		$(this).parent('.nav-item').toggleClass('open-child'); 
		$('body').toggleClass('active-sub-nav');
	}); 
  $(".header-area .navbar-toggler").click(function() {
    $('body').toggleClass("open-side-nav");  
  });
  $(".header-area .side-nav-area .close-btn").click(function() {
    $('body').removeClass("open-side-nav");  
  });
  
  /*-------------------------------------
            SIDEBAR POPUP 
  ---------------------------------------*/
  // SIDEBAR POPUP
  $(".poup-toggle").click(function(e){ 
    e.preventDefault();
    var IDVAL = $(this).attr('popup-toggle');
    var OpenEl = $(`#${IDVAL}`);  
    OpenEl.toggleClass('open');  
    $('body').toggleClass('open-sidebar'); 
  });
  $(".close-btn").click(function() {
    $(this).closest('.sidebar-popup-area').removeClass('open');
    $('body').removeClass('open-sidebar');   
  }); 
  /*-------------------------------------
            datepicker SCRIPTS 
  ---------------------------------------*/
  $('.input-datepicker input').each(function() {
      $(this).datepicker({
        autoclose: true,  
      });  
  });
  /*-------------------------------------
            MODAL SCRIPTS 
  ---------------------------------------*/
  $("[modal-toggle]").click(function() {
    var data = $(this).attr('modal-toggle');
    $(`#${data}`).addClass('open'); 
    $("body").addClass('overflow-hidden open-modal'); 
  });
  $(".module-area .close-btn").click(function() {
    $(this).closest('.module-area').removeClass('open');
    $("body").removeClass('overflow-hidden open-modal');   
  });
/*-------------------------------------
        
---------------------------------------*/
$(".add-member-area .add-member-btn").click(function(){
  $(this).closest('.add-member-area').toggleClass('open'); 
});
$(".custom-dropdown-area .close-btn").click(function(event) {
  $(this).closest('.add-member-area').removeClass('open');  
});
$(".custom-dropdown-area .btn").click(function(event) { 
  $(this).closest('.add-member-area').removeClass('open');  
});
$( ".custom-dropdown-area").each(function() { 
  $(this).draggable({ handle: ".header" }); 
});

$(".custom-dropdown-wrapper .dropdown-link").click(function(){
  $(this).closest('.custom-dropdown-wrapper').toggleClass('open'); 
});
$(".custom-dropdown-wrapper .btn").click(function(){ 
  $(this).closest('.custom-dropdown-wrapper').removeClass('open'); 
});
/*-------------------------------------
        Add Category
---------------------------------------*/
$("#add-category").each(function(){ 
  $(this).inputTags({
    max: 5,     
  });  
});
/*-------------------------------------
      CHANGE STATUSBAR DROPDOWN
---------------------------------------*/
$(".select--dropdown--area .data-list .list-item .status-bar").on('click', function(event) {
  var data = $(this);  
  var Result = $(this).closest('.data-list').parent('.select--dropdown--area').find('.result span');
  var Input = $(this).closest('.data-list').parent('.select--dropdown--area').find('.data-field'); 
  var DataText = data.text(); 
  var DataStyle = data.attr('class');
  Result.attr('class', ''); 
  Result.attr('class',DataStyle); 
  Result.text(DataText) 
  Input.val(DataText); 
  console.log(DataStyle); 
});

$(".select--dropdown--area .result-area").click(function(event) {
  $(this).closest('.select--dropdown--area').toggleClass('open'); 
});



});



