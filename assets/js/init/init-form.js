$(document).ready(function() {
	// CUSTOM SELECT OPTION
	$(".select-field select").each(function(){
		$(this).chosen({
			disable_search_threshold: 10, 
			placeholder_text_single: "Select an option",
			no_results_text: "Oops, nothing found!" 
		});  
	}); 

	$(".select-2-option").select2();   
	$(".select-2-tag").select2({ 
		containerCssClass : "Test------s"
	});   

	// DROPDOWN SCRIPTS 
	function formatState (state) {
	  if (!state.id) {
	    return state.text;
	  } 
	  var DataVal = state.element.getAttribute('data-num');
	  if (!DataVal){
	  	var DataNum = ' '; 
	  }else{
	  	var DataNum = '<span class="right-count">'+DataVal+'</span>';
	  }
	  var DataStyle = state.element.getAttribute('data-style');
	  if (!DataStyle){ 
	  	var AllClass = '';
	  }else{
	  	var AllClass = 'status-bar text-left has-right w-100';
	  }
	  var $state = $(
	    '<span class=" '+AllClass+' '+DataStyle+'">' + state.text +''+DataNum+'</span>' 
	  );
	  return $state;
	};
	$(".select-2-dropdown").each(function() {
		$(this).select2({
			templateResult: formatState
		});  
	});

	// console.log(formatState());  
	
});