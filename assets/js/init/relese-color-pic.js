$(document).ready(function() {
	$(".release-color-pic-area .add-color").click(function() {
		var Position = $(this).position(); 
		$(".release-color-pic-area .color-pic-box").css({
			left: Position.left,
			top: Position.top,
		});
 		$(".release-color-pic-area .color-pic-box").addClass('open');  
	});
	$(".release-color-pic-area .color-pic-box .color-box").click(function(){ 
		$(".release-color-pic-area .color-pic-box").removeClass('open'); 
	});
	$(".release-color-pic-area .add-release").click(function() { 
		$(this).parent(".release-color-pic-area").children('.release-color-list').append(
			`<li class="color-field-item">
               <div class="color-text-bar">Bugs</div>
               <span class="color-box add-color"></span>
               <button class="btn btn-success ml-2">Add</button> 
            </li> `
		)
	})
});		