$(function() { 
	  $("#datatable-responsive").DataTable({
	    aaSorting: [],
	    responsive: true,

	    columnDefs: [
	      {
	        responsivePriority: 1,
	        targets: 0
	      },
	      {
	        responsivePriority: 2,
	        targets: -1
	      }
	    ]
	  });
	  $(".active-datatable").DataTable({ 
	    aaSorting: [],
	    responsive: true,

	    columnDefs: [
	      {
	        responsivePriority: 1,
	        targets: 0
	      },
	      {
	        responsivePriority: 2,
	        targets: -1
	      }
	    ]
	  });
	  $(".dataTables_filter input").attr("placeholder", "Search…"); 
	  $(".th-none").css('display', 'none'); 
}); 